from __future__ import division
import time ; import torch
import torch.nn as nn
from torch.autograd import Variable
import numpy as np ; import cv2
from util import * ; import argparse
import os ; import os.path as osp
from darknet import Darknet
import pickle as pkl ; import pandas as pd ;import random
from kalman_sort import helper_counter as hc
from kalman_sort.helper_kalman import *

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=UserWarning)
args = hc.arg_parse()
batch_size  = int(args.bs)
confidence  = float(args.confidence)
nms_thesh   = float(args.nms_thresh)
start       = 0
CUDA        = torch.cuda.is_available()
num_classes = 80
classes     = load_classes("data/coco.names")

#Set up the neural network
print("Loading network.....")
model = Darknet(args.cfgfile)
model.load_weights(args.weightsfile)
print("Network successfully loaded")
model.net_info["height"] = args.reso
inp_dim = int(model.net_info["height"])
assert inp_dim % 32 == 0
assert inp_dim > 32

#If there's a GPU availible, put the model on GPU
if CUDA:
    model.cuda()
#Set the model in evaluation mode
model.eval()

videofile = args.videofile #or path to the video file.
cap       = cv2.VideoCapture(videofile)
frames    = 0
start     = time.time()
roi_mode  = args.roi_mode

assert cap.isOpened(), 'Cannot capture source'
if roi_mode == 1:
    pr = [0,0,int(cap.get(3)),int(cap.get(4))]
else:
    pr = roi_mode

with open('outputDetection.txt','w') as out_file:
    while cap.isOpened():
        ret, frame_ = cap.read()
        if ret:
            if frames == 0:
                mot_tracker = Sort()
            frame  = frame_[pr[1]:pr[3],pr[0]:pr[2]]
            frameX = cv2.rectangle(frame_,(pr[0],pr[1]),(pr[2],pr[3]),(0,0,0),2) #Rectangle of the ROI
            img    = prep_image(frame, inp_dim)
            im_dim = frame.shape[1], frame.shape[0]
            im_dim = torch.FloatTensor(im_dim).repeat(1,2)
            if CUDA:
                im_dim = im_dim.cuda()
                img = img.cuda()
            with torch.no_grad():
                output = model(Variable(img, volatile = True), CUDA)

            output = write_results(output, confidence, num_classes, nms_conf = nms_thesh)

            if type(output) == int:
                frames += 1
                # print("FPS of the video is {:5.4f}".format( frames / (time.time() - start)))
                cv2.imshow("frame", frameX)
                key = cv2.waitKey(1)
                if key & 0xFF == ord('q'):
                    break
                continue

            im_dim           = im_dim.repeat(output.size(0), 1)
            scaling_factor   = torch.min(416/im_dim,1)[0].view(-1,1)
            output[:,[1,3]] -= (inp_dim - scaling_factor*im_dim[:,0].view(-1,1))/2
            output[:,[2,4]] -= (inp_dim - scaling_factor*im_dim[:,1].view(-1,1))/2
            output[:,1:5]   /= scaling_factor

            for i in range(output.shape[0]):
                output[i, [1,3]] = torch.clamp(output[i, [1,3]], 0.0, im_dim[i,0])
                output[i, [2,4]] = torch.clamp(output[i, [2,4]], 0.0, im_dim[i,1])

            output[:,[1,3]] += pr[0]
            output[:,[2,4]] += pr[1]

            input_tracker = hc.kalman_format(output,frames)
            dets     = input_tracker[input_tracker[:,0]==frames,1:]
            trackers = mot_tracker.update(dets)
            trackers = trackers.astype(int)
            # # TODO: Combine confidence score of the detection with the tracker algorithm

            classes = load_classes('data/coco.names')
            colors  = pkl.load(open("pallete", "rb"))
            # print(frames)
            list(map(lambda x: hc.write2(x, frameX,colors,classes,frames,out_file), trackers))



            # print(input_tracker)
            cv2.imshow('frame', frameX)
            key = cv2.waitKey(1)
            if key & 0xFF == ord('q'):
                break
            frames += 1
            # print("FPS of the video is {:5.2f}".format( frames / (time.time() - start)))
        else:
            break
    cap.release()
