import numpy as np
from filterpy.kalman import KalmanFilter
import shapely
from shapely.geometry import Polygon,Point
from sklearn.utils.linear_assignment_ import linear_assignment
import kalman_sort.helper_kalman_config as cfg

class Sort(object):
  def __init__(self,max_age=1,min_hits=3):
    """
    Sets key parameters for SORT
    """
    self.max_age  = max_age
    self.min_hits = min_hits
    self.trackers = []
    self.frame_count = 0
    self.poin = cfg.point

  def update(self,dets):
    """
    Params:
      dets - a numpy array of detections in the format [[x1,y1,x2,y2,score],[x1,y1,x2,y2,score],...]
    Requires: this method must be called once for each frame even with empty detections.
    Returns the a similar array, where the last column is the object ID.

    NOTE: The number of objects returned may differ from the number of detections provided.
    """
    self.frame_count += 1
    #get predicted locations from existing trackers.
    trks   = np.zeros((len(self.trackers),self.poin*2))
    to_del = []
    ret    = []
    for t,trk in enumerate(trks):
        pos    = self.trackers[t].predict()
        # print(pos[0:4].reshape(1,4)[0])
        # trk[:] = [pos[0], pos[1], pos[2], pos[3]]
        trk[:] = pos[0:self.poin*2].reshape(1,self.poin*2)[0]
        if(np.any(np.isnan(pos))):
            to_del.append(t)
    trks = np.ma.compress_rows(np.ma.masked_invalid(trks))

    for t in reversed(to_del):
        self.trackers.pop(t)

    matched, unmatched_dets, unmatched_trks = associate_detections_to_trackers(dets,trks)

    #update matched trackers with assigned detections
    for t,trk in enumerate(self.trackers):
        if(t not in unmatched_trks):
            d = matched[np.where(matched[:,1]==t)[0],0]
            # print('detection = ',dets[d,:][0][0])
            # print('track = ',trk.kf.x[0])
            trk.update(dets[d,:])

    #create and initialise new trackers for unmatched detections
    for i in unmatched_dets:
        # print(dets[i,:])
        trk = KalmanBoxTracker(dets[i,:])
        self.trackers.append(trk)
    i = len(self.trackers)
    for trk in reversed(self.trackers):
        d = trk.get_state()[0]
        # print(d,[trk.id+1])
        if((trk.time_since_update < 1) and (trk.hit_streak >= self.min_hits or self.frame_count <= self.min_hits)):
            ret.append(np.concatenate((d,[trk.id+1])).reshape(1,-1)) # +1 as MOT benchmark requires positive
        i -= 1
        #remove dead tracklet
        if(trk.time_since_update > self.max_age):
            self.trackers.pop(i)
    if(len(ret)>0):
        return np.concatenate(ret)
    return np.empty((0,self.poin*2+1))

class KalmanBoxTracker(object):
  """
  This class represents the internel state of individual tracked objects observed as bbox.
  """
  count = 0
  def __init__(self,pos):
    """
    Initialises a tracker using initial bounding box.
    """
    #define constant velocity model
    self.point  = cfg.point ; point = cfg.point
    self.kf     = KalmanFilter(dim_x=point*4, dim_z=point*2)
    F           = np.concatenate([np.eye(point*2),np.eye(point*2)],axis=1)
    F_temp      = np.concatenate([np.zeros([point*2,point*2]),np.eye(point*2)],axis=1)
    self.kf.F   = np.concatenate([F,F_temp])

    H           = np.zeros([point*2,point*4])
    H[0:2*point,0:2*point] = np.eye(point*2)
    self.kf.H   = H

    self.kf.R = self.kf.R
    self.kf.P[point*2:,point*2:] *= 1000
    self.kf.P *= 10.
    self.kf.Q[point*2:,point*2:] *= 0.01

    self.kf.x[:point*2] = np.array(pos).reshape((point*2,1))
    self.time_since_update = 0
    self.id = KalmanBoxTracker.count
    KalmanBoxTracker.count += 1
    self.history = []
    self.hits = 0
    self.hit_streak = 0
    self.age = 0

  def update(self,bbox):
    """
    Updates the state vector with observed bbox.
    """
    self.time_since_update = 0
    self.history = []
    self.hits += 1
    self.hit_streak += 1
    self.kf.update(np.array(bbox).reshape((self.point*2,1)))

  def predict(self):
    """
    Advances the state vector and returns the predicted bounding box estimate.
    """
    if((self.kf.x[6]+self.kf.x[2])<=0):
      self.kf.x[6] *= 0.0

    self.kf.predict()
    self.age += 1
    if(self.time_since_update>0):
      self.hit_streak = 0
    self.time_since_update += 1
    self.history.append((self.kf.x))
    return self.history[-1]

  def get_state(self):
    """
    Returns the current bounding box estimate.
    """
    return (np.array(self.kf.x[:self.point*2]).reshape((1,self.point*2)))


def degree(p1,p2):
    angle = np.rad2deg(np.arctan2((p2[1]-p1[1]),(p2[0]-p1[0])))
    if (p2[1]-p1[1])<0:
        angle +=360
    ang = angle
    if (135 > ang > 45) :
        ang = 0
    elif (225 < ang < 315):
        ang = 0
    elif (225>ang>135):
        ang = 90
    elif ang<45 or ang>315:
        ang = 90

    return ang
def iou(det,track):
    pairs = cfg.pairs
    roi_w =  cfg.roi_w ; roi_h =  cfg.roi_h ; start_pos = cfg.start_pos
    list_poly_t = []
    list_poly_d = []
    for pair in pairs:
        det[pair[0]]
        dx0,dy0 = (int(det[pair[0]*2] ),
                 int(det[pair[0]*2+1] ))
        dx1,dy1 = (int(det[pair[1]*2] ),
                 int(det[pair[1]*2+1] ))

        if 0 in np.array([dx0,dy0,dx1,dy1]):
            continue

        tx0,ty0 = (int(track[pair[0]*2] ),
                 int(track[pair[0]*2+1] ))
        tx1,ty1 = (int(track[pair[1]*2] ),
                 int(track[pair[1]*2+1] ))

        distance = np.sqrt((dx1 - dx0)**2 + (dy1 - dy0)**2)
        scale    = int(0.2*distance)
        ang      = degree((dx0,dy0),(dx1,dy1))

        x_atas_1  = dx0 + np.cos(ang) * scale ; y_atas_1  = dy0 + np.sin(ang) * scale
        x_atas_2  = dx1 + np.cos(ang) * scale ; y_atas_2  = dy1 + np.sin(ang) * scale
        x_bawah_1 = dx0 - np.cos(ang) * scale ; y_bawah_1 = dy0 - np.sin(ang) * scale
        x_bawah_2 = dx1 - np.cos(ang) * scale ; y_bawah_2 = dy1 - np.sin(ang) * scale
        poly = Polygon([(x_atas_1,y_atas_1),(x_atas_2,y_atas_2),(x_bawah_2,y_bawah_2),(x_bawah_1,y_bawah_1)])
        list_poly_d.append(poly)

        distance = np.sqrt((tx1 - tx0)**2 + (ty1 - ty0)**2)
        scale    = int(0.2*distance)
        ang      = degree((tx0,ty0),(tx1,ty1))

        x_atas_1  = tx0 + np.cos(ang) * scale ; y_atas_1  = ty0 + np.sin(ang) * scale
        x_atas_2  = tx1 + np.cos(ang) * scale ; y_atas_2  = ty1 + np.sin(ang) * scale
        x_bawah_1 = tx0 - np.cos(ang) * scale ; y_bawah_1 = ty0 - np.sin(ang) * scale
        x_bawah_2 = tx1 - np.cos(ang) * scale ; y_bawah_2 = ty1 - np.sin(ang) * scale
        poly = Polygon([(x_atas_1,y_atas_1),(x_atas_2,y_atas_2),(x_bawah_2,y_bawah_2),(x_bawah_1,y_bawah_1)])
        list_poly_t.append(poly)

    # area = list_poly_d[0].intersection(list_poly_d[0]).area
    # print(area)
    iou_sum = 0
    for i in range(len(list_poly_d)):
        area = list_poly_t[i].intersection(list_poly_d[i]).area
        try:
            iuo = area/(((list_poly_t[i].area+list_poly_d[i].area))-area)
            iou_sum += iuo
        except ZeroDivisionError:
            iuo = 0
            continue
    try:
        average_iou = iou_sum/len(list_poly_d)
    except ZeroDivisionError:
        average_iou = 0
    # print('average_iou = ',average_iou)
    # print('area = ',area)
    # print('iuo = ',iuo)
    return average_iou

def associate_detections_to_trackers(detections,trackers,iou_threshold = 0.2):
    poin = cfg.point
    if(len(trackers)==0):
        return np.empty((0,2),dtype=int), np.arange(len(detections)), np.empty((0,poin*2+1),dtype=int)
    iou_matrix = np.zeros((len(detections),len(trackers)),dtype=np.float32)

    for d,det in enumerate(detections):
        for t,trk in enumerate(trackers):
            iou_matrix[d,t] = iou(det,trk)

    # print(iou_matrix)
    matched_indices = linear_assignment(-iou_matrix)
    unmatched_detections = []

    for d,det in enumerate(detections):
        if(d not in matched_indices[:,0]):
            unmatched_detections.append(d)
    unmatched_trackers = []
    for t,trk in enumerate(trackers):
        if(t not in matched_indices[:,1]):
            unmatched_trackers.append(t)
    #filter out matched with low IOU
    matches = []
    for m in matched_indices:
        if(iou_matrix[m[0],m[1]]<iou_threshold):
            unmatched_detections.append(m[0])
            unmatched_trackers.append(m[1])
        else:
            matches.append(m.reshape(1,2))
    if(len(matches)==0):
        matches = np.empty((0,2),dtype=int)
    else:
        matches = np.concatenate(matches,axis=0)
    return matches, np.array(unmatched_detections), np.array(unmatched_trackers)
