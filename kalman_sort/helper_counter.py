import argparse
import random
import cv2
import numpy as np

def arg_parse():
    """
    Parse arguements to the detect module

    """

    parser = argparse.ArgumentParser(description='YOLO v3 Detection Module')
    parser.add_argument("--bs", dest = "bs", help = "Batch size", default = 1)
    parser.add_argument("--confidence", dest = "confidence", help = "Object Confidence to filter predictions", default = 0.5)
    parser.add_argument("--nms_thresh", dest = "nms_thresh", help = "NMS Threshhold", default = 0.4)
    parser.add_argument("--cfg", dest = 'cfgfile', help ="Config file",default = "cfg/yolov3.cfg", type = str)
    parser.add_argument("--weights", dest = 'weightsfile', help ="weightsfile",default = "weight/yolov3.weights", type = str)
    parser.add_argument("--reso", dest = 'reso', help ="Input resolution of the network. Increase to increase accuracy. Decrease to increase speed",default = "416", type = str)
    parser.add_argument("--video", dest = "videofile", help = "Video file to run detection on", default = "AVG-TownCentre.mp4", type = str)
    parser.add_argument("--roi_mode",dest='roi_mode',default =1,nargs='+',type = int)
    return parser.parse_args()

def write(x, results,colors,classes,frames,out_file):
    # print(x[1:3])
    c1 = tuple(x[1:3].astype(int))
    c2 = tuple(x[3:5].astype(int))
    img = results
    cls = int(x[-1])
    color = random.choice(colors)
    label = "{0}".format(classes[cls])
    cv2.rectangle(img, c1, c2,color, 1)
    t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1 , 1)[0]
    c2 = c1[0] + t_size[0] + 3, c1[1] + t_size[1] + 4
    cv2.rectangle(img, c1, c2,color, -1)
    print('%d,-1,%.2f,%.2f,%.2f,%.2f,%s'%(frames,x[1],x[2],x[3]-x[1],x[4]-x[2],label),file=out_file)
    cv2.putText(img, label, (c1[0], c1[1] + t_size[1] + 4), cv2.FONT_HERSHEY_PLAIN, 1, [225,255,255], 1);
    return img
def write2(x, results,colors,classes,frames,out_file):
    # print(x[1:3])
    c1 = tuple(x[0:2].astype(int))
    # print(c1)
    c2 = tuple(x[2:4].astype(int))
    # print(c2)
    img = results
    cls = int(x[-1])
    color = random.choice(colors)
    label = str(cls)
    cv2.rectangle(img, c1, c2,color, 1)
    t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1 , 1)[0]
    c2 = c1[0] + t_size[0] + 3, c1[1] + t_size[1] + 4
    cv2.rectangle(img, c1, c2,color, -1)
    print('%d,-1,%.2f,%.2f,%.2f,%.2f,%s'%(frames,x[1],x[2],x[3]-x[1],x[4]-x[2],label),file=out_file)
    cv2.putText(img, label, (c1[0], c1[1] + t_size[1] + 4), cv2.FONT_HERSHEY_PLAIN, 1, [225,255,255], 1);
    return img

def kalman_format(output,f_numb):
    x = output.cpu().numpy()
    index = np.ones([1,x.shape[0]]).astype(int)*f_numb
    y = np.concatenate((index.T,x),axis = 1)
    y = np.delete(y,(1,6,7,8),1).astype(int)
    return y
