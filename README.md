## Implement object detection as backbone of counting
#### **A. YOLO_V3 as detector**
  ![Detection Example](https://i.imgur.com/m2jwnen.png)
  **Why YOLO? Because fast and quite reliable**
>  You can use original repository from the YOLO original version [pjreddie] <'https://pjreddie.com/darknet/yolo/'> as well as the weight can be download from the web.
However,you must make some modification from the original format output of YOLO to be able feed into the counting algorithm.

> You can directly clone this repository, but you must install the dependency related to the framework of this repo. The weight file can be download from http://bit.ly/2Iv0Dif. This weight was cloned from pjreddie. Placed on **'weight'** folder.

> TEST your detector work as it should be
```bash
foo@bar:~$ python video.py --video your_video.mp4
```
if you want to try to fed a single image you can use
```bash
foo@bar:~$ python image.py --image your_image.png
```

> If you want to use ROI mode. which means Calculate some specific area on the video / image. You can try to run mouse_event_opencv.py as the coordinator extractor. You can click where the rectangle start and stop at 2 points after the video start. The results will be print at the terminal.
```bash
foo@bar:~$ python mouse_event_opencv.py --video your_video.mp4
```

The result of this code can easily feed to the main program with --roi_mode
```bash
foo@bar:~$ python video.py --video your_video.mp4 --roi_mode 224 399 506 661
```
## Implement counter using SORT algorithm
```bash
foo@bar:~$ python video_count.py --video your_video.mp4 --roi_mode 224 399 506 661
```
## Progress
:ballot_box_with_check::ballot_box_with_check::ballot_box_with_check::ballot_box_with_check::ballot_box_with_check::ballot_box_with_check::ballot_box_with_check::ballot_box_with_check::black_square_button::black_square_button:

> 1. Stable counter   :hourglass:
> 2. Live Demos       :hourglass:
### **REFERENCE**
  1. https://github.com/ayooshkathuria/YOLO_v3_tutorial_from_scratch
  2. https://pjreddie.com/darknet/yolo/
  3. https://github.com/abewley/sort
  4. -----
